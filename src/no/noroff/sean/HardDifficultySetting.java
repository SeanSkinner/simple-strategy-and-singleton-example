package no.noroff.sean;

public class HardDifficultySetting implements DifficultySetting {
    @Override
    public double getDamageModifier() {
        return 1.6;
    }
}
