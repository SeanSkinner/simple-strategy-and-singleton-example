package no.noroff.sean;

public enum DifficultySettingManager {
    INSTANCE;

    public DifficultySetting getDifficultySetting() {
        return INSTANCE.difficultySetting;
    }

    public void setDifficultySetting(DifficultySetting difficultySetting) {
        this.difficultySetting = difficultySetting;
    }

    private DifficultySetting difficultySetting;


}
