package no.noroff.sean;

public class ExpertDifficultySetting implements DifficultySetting {
    @Override
    public double getDamageModifier() {
        return 2.0;
    }
}
