package no.noroff.sean;

public interface DifficultySetting {
    double getDamageModifier();
}
