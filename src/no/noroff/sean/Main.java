package no.noroff.sean;

import no.noroff.sean.hero.Hero;
import no.noroff.sean.hero.reduction.MediumStrengthResistance;
import no.noroff.sean.hero.reduction.SoftStrengthResistance;

public class Main {

    public static void main(String[] args) {
        var softStrengthResistance = new SoftStrengthResistance();
        var mediumStrengthResistance = new MediumStrengthResistance();
        DifficultySettingManager.INSTANCE.setDifficultySetting(new ExpertDifficultySetting());

        var playerCharacter = new Hero(100, 10, 10, 10, mediumStrengthResistance, false);
        var enemyCharacter = new Hero(100, 10, 10, 10, softStrengthResistance, true);

        System.out.println(playerCharacter);

        enemyCharacter.attack(playerCharacter);

        System.out.println(playerCharacter);

        DifficultySettingManager.INSTANCE.setDifficultySetting(new NormalDifficultySetting());
    }
}
