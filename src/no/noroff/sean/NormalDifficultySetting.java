package no.noroff.sean;

public class NormalDifficultySetting implements DifficultySetting {

    @Override
    public double getDamageModifier() {
        return 1;
    }
}
