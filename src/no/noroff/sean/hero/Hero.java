package no.noroff.sean.hero;

import no.noroff.sean.DifficultySettingManager;
import no.noroff.sean.hero.reduction.DamageResistance;

public class Hero {
    private int health;
    private int strength;
    private int dexterity;
    private int intelligence;

    private boolean isEnemy;

    private DamageResistance damageResistance;

    public Hero(int health, int strength, int dexterity, int intelligence, DamageResistance damageResistance, boolean isEnemy) {
        this.health = health;
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
        this.damageResistance = damageResistance;
        this.isEnemy = isEnemy;
    }

    public void attack(Hero target) {
        var damage = isEnemy ? 30 * calculateDifficultyScaling() : 30;
        target.takeDamage((int) damage);
    }

    public void takeDamage(int incomingDamage) {
        health -= incomingDamage * calculateDamageResistance();
    }

    public double calculateDamageResistance() {
        return damageResistance.getResistance(strength, dexterity, intelligence);
    }

    public double calculateDifficultyScaling() {
       return DifficultySettingManager.INSTANCE.getDifficultySetting().getDamageModifier();
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    public void setDexterity(int dexterity) {
        this.dexterity = dexterity;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }

    public DamageResistance getDamageResistance() {
        return damageResistance;
    }

    public void setDamageResistance(DamageResistance damageResistance) {
        this.damageResistance = damageResistance;
    }

    @Override
    public String toString() {
        return "Hero{" +
                "health=" + health +
                ", strength=" + strength +
                ", dexterity=" + dexterity +
                ", intelligence=" + intelligence +
                ", damageResistance=" + damageResistance +
                '}';
    }
}
