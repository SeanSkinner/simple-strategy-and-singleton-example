package no.noroff.sean.hero.reduction;

public class SoftStrengthResistance implements DamageResistance {
    @Override
    public double getResistance(int strength, int dexterity, int intelligence) {
        return 1 / (double) strength * 5;
    }
}
