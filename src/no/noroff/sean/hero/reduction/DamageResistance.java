package no.noroff.sean.hero.reduction;

public interface DamageResistance {
    double getResistance(int strength, int dexterity, int intelligence);
}
